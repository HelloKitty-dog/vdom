# 正则中的角色

test:regexp.test(已知字符串)

march:已知字符串.march(regexp)

### 角色类
\d （“ d”来自“数字”）: 一个数字：从0到的字符9。

\s （“ s”来自“空间”）: 的空间符号：包括空格，制表符\t，换行符\n和其他一些罕见的字符，如\v，\f和\r。

\w （“ w”来自“单词”）: “单词”字符：拉丁字母或数字或下划线_。非拉丁字母（如西里尔字母或印地文）不属于\w。
例如，\d\s\w表示“数字”后跟“空格字符”后跟“文字字符”，例如1 a。

正则表达式可以同时包含常规符号和字符类。
例如，CSS\d将字符串CSS后跟数字匹配：
```
let str = "Is there CSS4?";
let regexp = /CSS\d/

alert( str.match(regexp) ); // CSS4
```
### 逆类
对于每个字符类，都有一个“逆类”，用相同的字母表示，但大写。

\D : 非数字：除以外的任何字符\d，例如字母。
\S : 非空格：除以外的任何字符\s，例如字母。
\W : 非文字字符：除以外的任何字符\w，例如非拉丁字母或空格。


如何从一个字符串中创建一个仅包含数字的电话号码+7(903)-123-45-67：：找到所有数字并加入它们。
```
let str = "+7(903)-123-45-67";
alert( str.match(/\d/g).join('') ); // 79031234567
```
另一种较短的替代方法是查找非数字\D并将其从字符串中删除：
```
let str = "+7(903)-123-45-67";
alert( str.replace(/\D/g, "") ); // 79031234567
```

点是“任何字符” : 点.是一种特殊字符类，可与“除换行符之外的任何字符”匹配。


请注意，点表示“任何字符”，而不是“缺少字符”。必须有一个与之匹配的字符：
```
alert( "CS4".match(/CS.4/) ); // null, no match because there's no character fo
```

与任何带有“ s”标志的字符一样
默认情况下，点与换行符不匹配\n。

例如，regexp A.B匹配A，然后B在它们之间使用任何字符，除了换行符\n：
```
alert( "A\nB".match(/A.B/) ); // null (no match)
```
在许多情况下，当我们希望用点来表示“任何字符”（包括换行符）时。
这就是标志的s作用。如果有正则表达式，则点.实际上匹配任何字符：
```
alert( "A\nB".match(/A.B/s) ); // A\nB (match!)
```
### 锚点（Anchors)：字符串开始 ^ 和末尾 $

插入符号 ^ 和美元符号 $ 在正则表达式中具有特殊的意义。它们被称为“锚点”。

插入符号 ^ 匹配文本开头，而美元符号 $ － 则匹配文本末尾。
举个例子，让我们测试一下文本是否以 Mary 开头：
```
let str1 = "Mary had a little lamb";
alert( /^Mary/.test(str1) ); // true
```
该模式 ^Mary 的意思是：字符串开始，接着是 “Mary”。

与此类似，我们可以用 snow$ 来测试文本是否以 snow 结尾:
```
let str1 = "it's fleece was white as snow";
alert( /snow$/.test(str1) ); // true
```

### Flag "m" -- 多行模式
通过 flag /.../m 可以开启多行模式。

这仅仅会影响 ^ 和 $ 锚符的行为。在多行模式下，它们不仅仅匹配文本的开始与结束，还匹配每一行的开始与结束。

在这个有多行文本的例子中，正则表达式 /^\d+/gm 将匹配每一行的开头数字：
```
let str = `1st place: Winnie
2nd place: Piglet
33rd place: Eeyore`;
alert( str.match(/^\d+/gm) ); // 1, 2, 33
```
没有 flag /.../m 时，仅仅是第一个数字被匹配到：
```
let str = `1st place: Winnie
2nd place: Piglet
33rd place: Eeyore`;
alert( str.match(/^\d+/g) ); // 1
```

### 词边界

词边界：\b  
词边界 \b 是一种检查，就像 ^ 和 $ 一样。

当正则表达式引擎（实现搜索正则表达式的程序模块）遇到 \b 时，它会检查字符串中的位置是否是词边界。

有三种不同的位置可作为词边界：
+ 在字符串开头，如果第一个字符是单词字符 \w。
+ 在字符串中的两个字符之间，其中一个是单词字符 \w，另一个不是。
+ 在字符串末尾，如果最后一个字符是单词字符 \w。

例如，可以在 Hello, Java! 中找到匹配 \bJava\b 的单词，其中 Java 是一个独立的单词，而在 Hello, JavaScript! 中则不行。


\b 既可以用于单词，也可以用于数字。

例如，模式 \b\d\d\b 查找独立的两位数。换句话说，它查找的是两位数，其周围是与 \w 不同的字符，例如空格或标点符号（或文本开头/结尾）。
```
alert( "1 23 456 78".match(/\b\d\d\b/g) ); // 23,78
alert( "12,34,56".match(/\b\d\d\b/g) ); // 12,34,56
```

### 转义，特殊字符

+ 要在字面（意义）上搜索特殊字符 [ \ ^ $ . | ? * + ( )，我们需要在它们前面加上反斜杠 \（“转义它们”）。
+ 如果我们在 /.../ 内部（但不在 new RegExp 内部），还需要转义 /。
+ 传递一个字符串（参数）给 new RegExp 时，我们需要双倍反斜杠 \\，因为字符串引号会消费其中的一个。

### 集合和范围
##### 集合
比如说，[eao] 意味着查找在 3 个字符 'a'、'e' 或者 `‘o’ 中的任意一个。

这被叫做一个集合。集合可以在正则表达式中和其它常规字符一起使用。

##### 范围
方括号也可以包含字符范围。

比如说，[a-z] 会匹配从 a 到 z 范围内的字母，[0-5] 表示从 0 到 5 的数字。

在下面的示例中，我们会查询首先匹配 "x" 字符，再匹配两个数字或者位于 A 到 F 范围内的字符。

### 量词 `+,*,?` 和 `{n}`

###### 数量 {n}
确切的位数：{5}
\d{5} 表示 5 位的数字，如同 \d\d\d\d\d。

某个范围的位数：{3,5}
我们可以将限制范围的数字放入括号中，来查找位数为 3 至 5 位的数字：\d{3,5}

##### 缩写
+：代表“一个或多个”，相当于 {1,}。

?：代表“零个或一个”，相当于 {0,1}。换句话说，它使得符号变得可选。

*：代表着“零个或多个”，相当于 {0,}。也就是说，这个字符可以多次出现或不出现。

### 贪婪量词和惰性量词

###### 贪婪搜索
为了查找到一个匹配项，正则表达式引擎采用了以下算法：

+ 对于字符串中的每一个字符
+ 用这个模式来匹配此字符。
+ 若无匹配，移至下一个字符

例子：
```
let reg = /".+"/g;
let str = 'a "witch" and her "broom" is one';
alert( str.match(reg) ); // "witch" and her "broom"
```
…我们会发现它的运行结果与预期不同！

它直接找到了一个匹配结果："witch" and her "broom"，而不是找到两个匹配结果 "witch" 和 "broom"。

总结来说就是匹配到了一个符合要求的字符串不会停止，而是继续匹配到结束

###### 懒惰模式
懒惰模式中的量词与贪婪模式中的是相反的。它想要“重复最少次数”。

我们能够通过在量词之后添加一个问号 '?' 来启用它，所以匹配模式变为 *? 或 +?，甚至将 '?' 变为 ??。

这么说吧：通常，一个问号 ? 就是一个它本身的量词（0 或 1），但如果添加另一个量词（甚至可以是它自己），就会有不同的意思 —— 它将匹配的模式从贪婪转为懒惰。

这样上述代码就符合要求了
```
let reg = /".+?"/g;
let str = 'a "witch" and her "broom" is one';
alert( str.match(reg) ); // witch, broom
```