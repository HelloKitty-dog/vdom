# js事件节流和防抖

#### 防抖：当持续触发事件时，一定时间段内没有再触发事件，事件处理函数才会执行一次，如果设定的时间到来之前，又一次触发了事件，就重新开始延时。

比如一个滚动事件，我们想让执行滚动事件后页面打印一个数字，但是滚动事件只要你稍微滚动鼠标就会触发很多次，降低浏览器性能，因此我们使用防抖函数，当用户滚动鼠标后在设定的时间间隔t秒内用户不在做滚动鼠标动作那么数字会被顺利打印，但是如果用户在t秒内不断滚动鼠标那么打印事件将不会被触发，直到t秒间隔之后。

![avatar](https://crm-demo1.oss-cn-beijing.aliyuncs.com/images/%E9%98%B2%E6%8A%96.jpg)

定义防抖函数：
```
function debounce(fn,delay){
    let timer = null //借助闭包
    return function() {
        if(timer){
            clearTimeout(timer) 
        }
        timer = setTimeout(fn,delay) // 简化写法
    }
}
```
防抖函数的应用
```
//事件处理函数（打印数字）
function handle() {    
    console.log(Math.random()); 
}
// 监听滚动事件
window.addEventListener('scroll', debounce(handle, 1000));
```
当持续触发scroll事件时，事件处理函数handle只在停止鼠标滚动后1000毫秒才会调用一次，也就是说在持续触滚动鼠标的过程中，事件处理函数handle一直没有执行。

#### 节流：相比于防抖函数频繁点击提交事件始终不会触发，节流函数是指事件函数在某段时间内只执行一次。

比如我们让他在每t秒时间间隔的最后时刻才执行，类似于王者荣耀里面英雄的技能冷却。

![avatar](https://crm-demo1.oss-cn-beijing.aliyuncs.com/images/%E8%8A%82%E6%B5%81.jpg)


函数节流主要有两种实现方法：时间戳和定时器。接下来分别用两种方法实现throttle

+ 节流throttle代码（时间戳）：

```
var throttle = function(func, delay) {            
　　var prev = Date.now();            
　　return function() {                
　　　　var context = this;                
　　　　var args = arguments;                
　　　　var now = Date.now();                
　　　　if (now - prev >= delay) {                    
　　　　　　func.apply(context, args);                    
　　　　　　prev = Date.now();                
　　　　}            
　　}        
}        
function handle() {            
　　console.log(Math.random());        
}        
window.addEventListener('scroll', throttle(handle, 1000));
```

当高频事件触发时，第一次会立即执行（给scroll事件绑定函数与真正触发事件的间隔一般大于delay，而后再怎么频繁地触发事件，也都是每delay时间才执行一次。而当最后一次事件触发完毕后，事件也不会再被执行了 （因为你的时间间隔是精确的必定会在delay之内，不在的话那就不叫最后一次了，如果在delay之内就不够一次时间间隔，除非你能准确的落到delay的最后时刻。

+ 节流throttle代码（定时器）：

```
// 节流throttle代码（定时器）：
var throttle = function(func, delay) {            
    var timer = null;            
    return function() {                
        var context = this;               
        var args = arguments;                
        if (!timer) {                    
            timer = setTimeout(function() {                        
                func.apply(context, args);                        
                timer = null;                    
            }, delay);                
        }            
    }        
}        
function handle() {            
    console.log(Math.random());        
}        
window.addEventListener('scroll', throttle(handle, 1000));
```

当触发事件的时候，我们设置一个定时器，再次触发事件的时候，如果定时器存在，就不执行，直到delay时间后，定时器执行执行函数，并且清空定时器，这样就可以设置下个定时器。当第一次触发事件时，不会立即执行函数，而是在delay秒后才执行。而后再怎么频繁触发事件，也都是每delay时间才执行一次。当最后一次停止触发后，由于定时器的delay延迟，可能还会执行一次函数。

节流中用时间戳或定时器都是可以的。更精确地，可以用时间戳+定时器，当第一次触发事件时马上执行事件处理函数，最后一次触发事件后也还会执行一次事件处理函数。

+ 节流throttle代码（时间戳+定时器）：

```
// 节流throttle代码（时间戳+定时器）：
var throttle = function(func, delay) {     
    var timer = null;     
    var startTime = Date.now();     
    return function() {             
        var curTime = Date.now();             
        var remaining = delay - (curTime - startTime);             
        var context = this;             
        var args = arguments;             
        clearTimeout(timer);              
        if (remaining <= 0) {                    
            func.apply(context, args);                    
            startTime = Date.now();              
        } else {                    
            timer = setTimeout(func, remaining);              
        }      
    }
}
function handle() {      
    console.log(Math.random());
} 
window.addEventListener('scroll', throttle(handle, 1000));
```

在节流函数内部使用开始时间startTime、当前时间curTime与delay来计算剩余时间remaining，当remaining<=0时表示该执行事件处理函数了（保证了第一次触发事件就能立即执行事件处理函数和每隔delay时间执行一次事件处理函数）。如果还没到时间的话就设定在remaining时间后再触发 （保证了最后一次触发事件后还能再执行一次事件处理函数）。当然在remaining这段时间中如果又一次触发事件，那么会取消当前的计时器，并重新计算一个remaining来判断当前状态。


#### 总结

函数防抖：将几次操作合并为一次操作进行。原理是维护一个计时器，规定在delay时间后触发函数，但是在delay时间内再次触发的话，就会取消之前的计时器而重新设置。这样一来，只有最后一次操作能被触发。

函数节流：使得一定时间内只触发一次函数。原理是通过判断是否到达一定时间来触发函数。

区别： 函数节流不管事件触发有多频繁，都会保证在规定时间内一定会执行一次真正的事件处理函数，而函数防抖只是在最后一次事件后才触