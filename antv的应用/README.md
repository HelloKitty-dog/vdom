# antv--G2的应用

### 绘制点图
绘图首先要做数据准备，要和后端商讨后前端所需的数据类型，因为G2可以根据后端传过来的json数据来决定图表的横纵坐标以及legend表头
比如这种类型：
```
[
  { feature: 'Battery', value: 0.22, phone: 'iPhone' },
  { feature: 'Brand', value: 0.28, phone: 'iPhone' },
];
```
很明显这是json类型的数组

现在我们使用 G2 来绘制一张简单的点图，我们将 feature  和 value  两个字段分别映射至图表的 x 和 y 轴上：

+ 使用 point 几何标记绘制点图；
+ 使用 position  通道，将对应的变量映射到 x 和 y 位置空间上。

```
const data = [
  { feature: 'Battery', value: 0.22, phone: 'iPhone' },
  { feature: 'Brand', value: 0.28, phone: 'iPhone' },
];

const chart = new Chart({
  container: 'mountNode',
  autoFit: false,
  width: 600,
  height: 300,
});
chart.data(data);
chart.point().position('feature*value');
chart.render();
```

### 颜色、大小、形状等图形属性映射

#### 颜色
为了区分  iPhone，Samsung 以及  Nokia Smartphone 三款手机，我们将 phone  字段映射到 color 图形通道上，通过颜色来区分不同手机的数据。
```
chart
  .point()
  .position('feature*value')
  .color('phone');
  ```
这个时候，G2 会根据 phone  字段数值类型，自动生成一张图例，用以展示数据取值与图形属性之间的对应关系。

#### 形状

同样，我们可以通过点形状来区分不同手机的数据，所以我们又将 phone  字段映射至 shape 图形通道，并指定具体的 shape 形状：
```
chart
  .point()
  .position('feature*value')
  .color('phone')
  .shape('phone', ['circle', 'square', 'triangle']);
  ```
  #### 大小
  为了让点更清晰，我们通过 size 图形通道适当放大这些点。
```
chart
  .point()
  .position('feature*value')
  .color('phone')
  .shape('phone', ['circle', 'square', 'triangle'])
  .size(6);
  ```

  #### 几何标记
  几何标记即我们所说的点、线、面这些几何图形。G2 中并没有特定的图表类型（柱状图、散点图、折线图等）的概念，用户可以单独绘制某一种类型的图表，如饼图，也可以绘制混合图表，比如折线图和柱状图的组合。G2 生成的图表的类型，都是由几何标记决定的，比如：
```
geometry = 'point' //就可以绘制点图
geometry = 'line' //就可以绘制折线图
geometry = 'area' //就可以绘制面积图
```
比如绘制折线图我们只需要将 chart.point()  改为 chart.line()  即可：
```
chart
  .line()
  .position('feature*value')
  .color('phone');
  ```
  #### 叠加几何标记
  当然我们还可以进行几何标记的叠加以实现混合图表的绘制：
```
chart
  .area()
  .position('feature*value')
  .color('phone');
chart
  .line()
  .position('feature*value')
  .color('phone');
chart
  .point()
  .position('feature*value')
  .color('phone')
  .shape('circle');
  ```
我们还可以通过 chart.coordinate()  接口，一步将以上图形切换至极坐标系(雷达图)下：
```
chart.coordinate('polar');
```
我们还可以通过 adjust()  接口将数据进行调整，让数据以层叠的方式进行展示，即绘制层叠面积图：
```
chart.adjust('stack')
  ```

#### 分面
上面我们已经介绍了使用图形属性（颜色和形状）类比较不同分组的方法，它可以帮助我们将所有的数据组都绘制在同一张图表上。而分面提供了另外一种方法：将一份数据按照某个维度分隔成若干子集，然后创建一个图表的矩阵，将每一个数据子集绘制到图形矩阵的窗格中，比如上述的数据，如果以 phone  字段为分割维度，使用 rect  分面类型，那么将会得到三个折线图。

#### 图表样式配置
G2 绘制的图表由以下基本元素组成：
![avatar](https://crm-demo1.oss-cn-beijing.aliyuncs.com/images/antv.png)

+ Axis 坐标轴可以通过 chart.axis()  接口进行配置
+ Tooltip 提示信息可以通过 chart.tooltip()  接口进行配置
+ Legend 图例可以通过 chart.legend()  接口进行配置

如下代码所示，我们通过  chart.legend() 接口调整了图例的显示位置：
```
chart.legend({
  position: 'right-bottom',
});
```


