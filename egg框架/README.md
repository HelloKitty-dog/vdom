# egg框架
Egg.js 为企业级框架和应用而生，是为了实现node服务端语言的一种框架，Egg 的插件机制有很高的可扩展性，一个插件只做一件事（比如 Nunjucks 模板封装成了 egg-view-nunjucks、MySQL 数据库封装成了 egg-mysql）。Egg 通过框架聚合这些插件，并根据自己的业务场景定制配置，这样应用的开发成本就变得很低。

有如下特性
+ 提供基于 Egg 定制上层框架的能力
+ 高度可扩展的插件机制
+ 内置多进程管理
+ 基于 Koa 开发，性能优异
+ 框架稳定，测试覆盖率高
+ 渐进式开发

关于如何快速搭建一个egg框架可见<a href='https://eggjs.org/zh-cn/intro/quickstart.html'>官方文档</a>，
以及egg每部分的命名和作用
<a href='https://eggjs.org/zh-cn/basics/structure.html'>egg的目录结构</a>，我们会在已有框架的基础上，对继续开发其他页面进行分析总结。

使用egg框架封装一些功能的时候只需要将功能函数export导出即可，在其他文件中使用时不需要刻意的import导入，只需要在使用时加上文件名前缀即可。
#### egg框架的基本结构及其作用
+ app/router.js 用于配置 URL 路由规则，具体参见 Router。
+ app/controller/** 用于解析用户的输入，处理后返回相应的结果，具体参见 Controller。
+ app/service/** 用于编写业务逻辑层，可选，建议使用，具体参见 Service。
+ app/middleware/** 用于编写中间件，可选，具体参见 Middleware。
+ app/public/** 用于放置静态资源，可选，具体参见内置插件 egg-static。
+ app/extend/** 用于框架的扩展，可选，具体参见框架扩展。
+ config/config.{env}.js 用于编写配置文件，具体参见配置。
+ config/plugin.js 用于配置需要加载的插件，具体参见插件。
+ test/** 用于单元测试，具体参见单元测试。
+ app.js 和 agent.js 用于自定义启动时的初始化工作，可选，具体参见启动自定义。关于agent.js的作用参见Agent机制。

#### 由内置插件约定的目录：

+ app/public/** 用于放置静态资源，可选，具体参见内置插件 egg-static。
+ app/schedule/** 用于定时任务，可选，具体参见定时任务。
+ 若需自定义自己的目录规范，参见 Loader API

app/view/** 用于放置模板文件，可选，由模板插件约定，具体参见模板渲染。
app/model/** 用于放置领域模型，可选，由领域类相关插件约定，如 egg-sequelize。