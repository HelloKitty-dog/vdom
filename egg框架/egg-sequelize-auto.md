# egg之egg-sequelize-auto的使用

自动生成modal命令（自动生成时要注意数据库不同环境的host）

格式：sequelize-auto -h 数据库的IP地址 -d 数据库名 -u 用户名 -x 密码 -p 端口 -t 表名
#### 1.默认生成全部表，重复的会覆盖
```
egg-sequelize-auto -o 映射到文件的项目地址 -d 数据库名 -h 数据库host -u 数据库账号 -p 数据库端口 -x 数据库密码 -e mysql
```

#### 2.生成指定的表格,在上述基础上做了-t限制
```
egg-sequelize-auto -o 映射到文件的项目地址 -d 数据库名 -t 数据库的表名 -h 数据库host -u 数据库账号 -p 数据库端口 -x 数据库密码 -e mysql
```
```
create_time: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: DataTypes.NOW,
      get() {
        return moment(this.getDataValue('createTime')).format('YYYY-MM-DD HH:mm:ss');
      }
    },
    update_time: {
      type: DataTypes.TIME,
      allowNull: false,
      get() {
        return moment(this.getDataValue('updateTime')).format('YYYY-MM-DD HH:mm:ss');
      }
    },
  }, {
    tableName: 'mt_user_num',
     //为了避免自动生成创建时间和更新时间，也可以在config.default.js中统一定义
     timestamps: false,
  });
  ```

