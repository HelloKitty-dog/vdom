# egg之Sequelize的使用


1.create新增：
```
ctx.User.create({ firstName: "Jane", lastName: "Doe" });
```

2.findAll查找整个表： 
```
ctx.User.findAll()
```

3.findAll查找指定的属性：
```
Model.findAll({
  attributes: ['foo', 'bar']
});
```
4.findAll聚合：
```
Model.findAll({
  attributes: [
    'foo',
    [sequelize.fn('COUNT', sequelize.col('hats')), 'n_hats']
    'bar'
  ]
});
//SELECT foo, COUNT(hats) AS n_hats, bar FROM ...
```
使用聚合函数时，必须给它一个别名，以便能够从模型中访问它。在上面的示例中，您可以使用instance.n_hats来获得帽子数量

5.使用findAll避免列出所有的属性：
```
Model.findAll({
  attributes: {
    include: [
      [sequelize.fn('COUNT', sequelize.col('hats')), 'n_hats']
    ]
  }
});
```
6.使用findAll剔除指定的属性：
```
Model.findAll({
  attributes: { exclude: ['baz'] }
});
```
6.1 findByPk：该findByPk方法使用提供的主键仅从表中获得一个条目。
```
const project = await Project.findByPk(123);
```
6.2 findOne：该findOne方法获取它找到的第一个条目
```
onst project = await Project.findOne({ where: { title: 'My Title' } });
```
6.3 findOrCreate：除非找到一个满足的查询条件，否则会自动创建
```
const [user, created] = await User.findOrCreate({
  where: { username: 'sdepold' },
  defaults: {
    job: 'Technical Lead JavaScript'
  }
});
```
6.4 findAndCountAll：findAll和count的组合返回两个值
Count：整数：符合查询条件的所有记录和
Rows：数组：符合条件的所有记录
```
const { count, rows } = await Project.findAndCountAll({
  where: {
    title: {
      [Op.like]: 'foo%'
    }
  },
  offset: 10,
  limit: 2
});
```
7.使用where限制语句：
```
Post.findAll({
  where: {
    authorId: 2
  }
});
// SELECT * FROM post WHERE authorId = 2
```
8.where和in连用：
```
Post.findAll({
  where: {
    id: [1,2,3] // Same as using `id: { [Op.in]: [1,2,3] }`
  }
});
// SELECT ... FROM "posts" AS "post" WHERE "post"."id" IN (1, 2, 3);
```
9.where和逻辑运算符的连用：
```
const { Op } = require("sequelize");
Foo.findAll({
  where: {
    rank: {
      [Op.or]: {
        [Op.lt]: 1000,
        [Op.eq]: null
      }
    },
    // rank < 1000 OR rank IS NULL

    {
      createdAt: {
        [Op.lt]: new Date(),
        [Op.gt]: new Date(new Date() - 24 * 60 * 60 * 1000)
      }
    },
    // createdAt < [timestamp] AND createdAt > [timestamp]

    {
      [Op.or]: [
        {
          title: {
            [Op.like]: 'Boat%'
          }
        },
        {
          description: {
            [Op.like]: '%boat%'
          }
        }
      ]
    }
    // title LIKE 'Boat%' OR description LIKE '%boat%'
  }
});

Project.findAll({
  where: {
    name: 'Some Project',
    [Op.not]: [
      { id: [1,2,3] },
      {
        description: {
          [Op.like]: 'Hello%'
        }
      }
    ]
  }
});
```
上面将生成：
```
SELECT *
FROM `Projects`
WHERE (
  `Projects`.`name` = 'a project'
  AND NOT (
    `Projects`.`id` IN (1,2,3)
    OR
    `Projects`.`description` LIKE 'Hello%'
  )
)
```   
10.高级查询
```
Post.findAll({
  where: sequelize.where(sequelize.fn('char_length', sequelize.col('content')), 7)
});
// SELECT ... FROM "posts" AS "post" WHERE char_length("content") = 7

11.update方法：User.update({ lastName: "Doe" }, {
  where: {
    lastName: null
  }
});
//update "user" set "lastName"="Doe" where "lastname"=null
```
11.delete方法的使用
```
await User.destroy({
  where: {
    firstName: "Jane"
  }
});
```
要销毁所有TRUNCATE可以使用的SQL：
```
await User.destroy({
  truncate: true
});
```
12.实用方法count，sum，max，min
```
const amount = await Project.count({
  where: {
    id: {
      [Op.gt]: 25
    }
  }
});
//select count() where “id”>25
```
假设我们有三个用户，分别是10、5和40岁。
```
await User.max('age'); // 40
await User.max('age', { where: { age: { [Op.lt]: 20 } } }); // 10
await User.min('age'); // 5
await User.min('age', { where: { age: { [Op.gt]: 5 } } }); // 10
await User.sum('age'); // 55
await User.sum('age', { where: { age: { [Op.gt]: 5 } } }); // 50
```
13.限制和分页：
```
// Skip 5 instances and fetch the 5 after that
Project.findAll({ offset: 5, limit: 5 });
```
14.分组：
```
Project.findAll({ group: 'name' });
```
15.定购：
```
Subtask.findAll({
  order: [
    // Will order by max(age) DESC
    [sequelize.fn('max', sequelize.col('age')), 'DESC']
]});
```
16.sequelize.fn()和sequelize.col()方法：
将用户的用户名转换为大写
```
instance.update({
  username: sequelize.fn('upper', sequelize.col('username'))
});
```
17.Op运算符文档：
https://sequelize.org/master/manual/model-querying-basics.html#operators

18.get和set函数都是用在model中的方法，可以对参数进行修改
Get：存放到数据库中的值不会发生变化，但是如果对数据进行读取，会读取到自动调用get之后的方法，如想读取原值使用getDataValue（该方法会读取数据库的值）
```
const User = sequelize.define('user', {
  username: {
    type: DataTypes.STRING,
    get() {
      const rawValue = this.getDataValue(username);
      return rawValue ? rawValue.toUpperCase() : null;
    }
  }
});
const user = User.build({ username: 'SuperUser123' });
console.log(user.username); // 'SUPERUSER123'
console.log(user.getDataValue(username)); // 'SuperUser123'
```
Set：存放到数据库的值发生了改变，无论调用何种方法都是set()方法执行后的值
```
const User = sequelize.define('user', {
  username: DataTypes.STRING,
  password: {
    type: DataTypes.STRING,
    set(value) {
      this.setDataValue('password', hash(value));
    }
  }
});
const user = User.build({ username: 'someone', password: 'NotSo§tr0ngP4$SW0RD!' });
console.log(user.password); // '7cfc84b8ea898bb72462e78b4643cfccd77e9f05678ec2ce78754147ba947acc'
console.log(user.getDataValue(password)); // '7cfc84b8ea898bb72462e78b4643cfccd77e9f05678ec2ce78754147ba947acc'
```
注：由于get和set方法都是自动调用的为了解决这种弊端我们可以二者连用，这样可以做到存放到数据库的值是隐秘的，而读取到的值是正常的

19.model定义时可以使用的验证方法：
https://sequelize.org/master/manual/validations-and-constraints.html#per-attribute-validations

20.如果模型的特定字段被设置为不允许null (allowNull: false)，并且该值已经被设置为null，那么所有验证器将被跳过，并抛出一个ValidationError。
另一方面，如果它被设置为允许null (allowNull: true)，并且该值已经被设置为null，那么只有内置验证器将被跳过，而定制验证器仍将运行。
