# mac配置多个key


步骤：


1 ssh-keygen -t rsa -C "your email" 生成公钥和私钥

2 Enter file in which to save the key (/Users/administrator/.ssh/id_rsa):  id_rsa_xxx (自定义名称，保存key)

3 Enter passphrase (empty for no passphrase): 输入密码

4 ssh-add -K ~/.ssh/id_rsa_yw   (一劳永逸将私钥添加进 Mac 本身的钥匙串中，即 Keychain，如果失效的话需要重新执行该命令)

5 测试  ssh -T git@gitlab.com  ( 这里要输入项目的git@hostname)

6  cd ~/.ssh 下  touch config


