# TS和JS的区别

## ES和TS比较，持续更新
### 类
ES6语法:
```
    class Person{
        a(){
            alert(123)
        };
        b(){
            return 112233
        };
        constructor(aa=999){
            this.x=aa;
            this.bb=function(){
                alert(222)
            }
        };
    }
var p=new Person(1);
```
则x和bb是p的属性,而a和b在p的原型链上


TS语法：
```
    class Person{
        x:number;
        bb;
        a(){
            alert(123)
        };
        b(){
            return 112233
        };
        constructor(public aa:number=999){
            this.x=aa;
            this.bb=function(){
                alert(222)
            }
        };
    }
var p=new Person(1);
```
此时constructor中的x和bb必须事先被声明,且默认为public
形参aa默认为private
为private的无法在外部被使用,仅有为public的才能作为实例p的属性被调用



TS中类的继承：
```
class Me extends Person{
    p2p:any;
    constructor(b){
        super(b);
        this.p2p='666'
    };
    mover(){
        console.log('my mover','mover')
        // super.mover()
    }
}
var me:Person=new Me(66)
```
出现同名的属性和方法则以子类的为准,但是依然可以用 super.对应名的方式调用父类的内容
在子类的构造函数中用super()可以直接继承父类的构造函数


ES6中类的继承：
子类中构造函数新增的属性必须重新声明，对于继承的父类的函数，如果调用了新的属性，子类需要重写
![avatar](https://crm-demo1.oss-cn-beijing.aliyuncs.com/images/1389702-20191214195052942-1168471465.png)


## 以下这两个是TS的特性，es6中不存在

### 接口

让我们开发这个示例应用。这里我们使用接口来描述一个拥有firstName和lastName字段的对象。 在TypeScript里，只在两个类型内部的结构兼容那么这两个类型就是兼容的。 这就允许我们在实现接口时候只要保证包含了接口要求的结构就可以，而不必明确地使用implements语句。
```
interface Person {
    firstName: string;
    lastName: string;
}

function greeter(person: Person) {
    return "Hello, " + person.firstName + " " + person.lastName;
}

let user = { firstName: "Jane", lastName: "User" };

document.body.innerHTML = greeter(user);
```
### 类型注解

TypeScript里的类型注解是一种轻量级的为函数或变量添加约束的方式。 在这个例子里，我们希望greeter函数接收一个字符串参数。 然后尝试把greeter的调用改成传入一个数组：
```
function greeter(person: string) {
    return "Hello, " + person;
}

let user = [0, 1, 2];
document.body.innerHTML = greeter(user);
```
重新编译，你会看到产生了一个错误。
```
error TS2345: Argument of type 'number[]' is not assignable to parameter of type 'string'.
```