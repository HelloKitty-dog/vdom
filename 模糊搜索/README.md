# 模糊搜索


### 实现工具:  mybatis，spring

因为基于mybatis实现的mapper层sql语句只包含增删改查等基本功能我们需要自行添加下述sql语句(只需修改表名和字段名即可使用)
```
  <select id="searchAll" resultMap="BaseResultMap">
    select
    <include refid="Base_Column_List" />
    from contract
    where 1 = 1
    <if test="search != null and search != ''">
      AND CONCAT( id, hno, principal, money, describes, clause) LIKE CONCAT('%',#{search},'%')
    </if>
  </select>
  ```
毫无疑问，前端输入的search参数应该是一个String类型的字符串，那么我们应该以何种方式将字符串传递给后端？

### 实现的三种方式：

第一种是以对象的形式传入参数
http://localhost:8080/search

postman调试用的参数：
{
    "search": "张飞"
}

需要注意的是以对象的形式传入参数需要一个类来实现
```
public class BaseQueryParams {
    private String search;
}
```
```
   //搜索
    @PostMapping(value = "/search")
    public ServerRep searchAll(@RequestBody BaseQueryParams params){
        return ServerRep.SuccessDO(contractService.searchAll(params.getSearch()));
    }
    ```
第二种是以'?'拼接的方式实现
http://localhost:8080/search1?search=张飞
```
    //搜索
    @PostMapping(value = "/search1")
    public ServerRep searchAll(@RequestParam String search){
        return ServerRep.SuccessDO(contractService.searchAll(search));
    }
```

第三种是以{}的方式来实现(把参数当成url的一部分)，需要注意的是如果参数是汉字需要转换一下编码方式，encodeURIComponent()方法来实现。

转换前：http://localhost:8080/search2/张飞

转换后：http://localhost:8080/search2/%E5%BC%A0%E9%A3%9E
```
    //搜索
    @PostMapping(value = "/search2/{search}")
    public ServerRep searchAll1(@PathVariable String  search){
        return ServerRep.SuccessDO(contractService.searchAll(search));
    }
```

