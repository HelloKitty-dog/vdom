# webpack总结

## 1.入口起点
入口起点(entry point)指示 webpack 应该使用哪个模块，来作为构建其内部依赖图的开始。进入入口起点后，webpack 会找出有哪些模块和库是入口起点（直接和间接）依赖的。
每个依赖项随即被处理，最后输出到称之为 bundles 的文件中。可以通过在 webpack 配置中配置 entry 属性，来指定一个入口起点（或多个入口起点）。

#### 单个入口语法

(使用此语法在扩展配置时有失灵活性)

entry 属性的单个入口语法，是下面的简写：
```
const config = {
  entry: {
    main: './path/to/my/entry/file.js'
  }
};
```
#### 对象语法

(最可扩展的方式)

分离 应用程序(app) 和 第三方库(vendor) 入口
```
const config = {
  entry: {
    app: './src/app.js',
    vendors: './src/vendors.js'
  }
};
```
#### 多页面应用程序

```
const config = {
  entry: {
    pageOne: './src/pageOne/index.js',
    pageTwo: './src/pageTwo/index.js',
    pageThree: './src/pageThree/index.js'
  }
};
```
## 2.输出

在 webpack 中配置 output 属性的最低要求是，将它的值设置为一个对象，包括以下两点：

filename 用于输出文件的文件名。
目标输出目录 path 的绝对路径。

```
const config = {
  output: {
    filename: 'bundle.js',
    path: '/home/proj/public/assets'
  }
};
module.exports = config;
```
#### 多个入口起点

如果配置创建了多个单独的 "chunk"，则应该使用占位符(substitutions)来确保每个文件具有唯一的名称。
```
{
  entry: {
    app: './src/app.js',
    search: './src/search.js'
  },
  output: {
    filename: '[name].js',
    path: __dirname + '/dist'
  }
}
```
## 3.模式
模式分为两种development和production。

development：会将 process.env.NODE_ENV 的值设为 development。启用 NamedChunksPlugin 和 NamedModulesPlugin。

production：会将 process.env.NODE_ENV 的值设为 production。启用 FlagDependencyUsagePlugin, FlagIncludedChunksPlugin, ModuleConcatenationPlugin, NoEmitOnErrorsPlugin, OccurrenceOrderPlugin, SideEffectsFlagPlugin 和 UglifyJsPlugin.

#### loader

loader 让 webpack 能够去处理那些非 JavaScript 文件（webpack 自身只理解 JavaScript）。loader 可以将所有类型的文件转换为 webpack 能够处理的有效模块，然后你就可以利用 webpack 的打包能力，对它们进行处理。

示例：
例如，你可以使用 loader 告诉 webpack 加载 CSS 文件，或者将 TypeScript 转为 JavaScript。为此，首先安装相对应的 loader：
```
npm install --save-dev css-loader
npm install --save-dev ts-loader
```
然后指示 webpack 对每个 .css 使用 css-loader，以及对所有 .ts 文件使用 ts-loader：
```
module.exports = {
  module: {
    rules: [
      { test: /\.css$/, use: 'css-loader' },
      { test: /\.ts$/, use: 'ts-loader' }
    ]
  }
};
```
#### 三种使用 loader 的方式：

+ 配置（推荐）：在 webpack.config.js 文件中指定 loader。
+ 内联：在每个 import 语句中显式指定 loader。
+ CLI：在 shell 命令中指定它们。

i.配置：

使用module.exports配置，允许在webpack中配置多个loader，这事比较常用的一种方式
```
module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          {
            loader: 'css-loader',
            options: {
              modules: true
            }
          }
        ]
      }
    ]
  }
  ```
  ii.内联:

可以在 import 语句或任何等效于 "import" 的方式中指定 loader。使用 ! 将资源中的 loader 分开。分开的每个部分都相对于当前目录解析。
```
import Styles from 'style-loader!css-loader?modules!./styles.css';
```
 iii.cli：

你也可以通过 CLI 使用 loader：
```
webpack --module-bind jade-loader --module-bind 'css=style-loader!css-loader'
```
这会对 .jade 文件使用 jade-loader，对 .css 文件使用 style-loader 和 css-loader。

## 插件
插件可以用于执行范围更广的任务。插件的范围包括，从打包优化和压缩，一直到重新定义环境中的变量。插件接口功能极其强大，可以用来处理各种各样的任务。

想要使用一个插件，你只需要 require() 它，然后把它添加到 plugins 数组中。多数插件可以通过选项(option)自定义。你也可以在一个配置文件中因为不同目的而多次使用同一个插件，这时需要通过使用 new 操作符来创建它的一个实例。
```
const HtmlWebpackPlugin = require('html-webpack-plugin'); // 通过 npm 安装
const webpack = require('webpack'); // 用于访问内置插件

const config = {
  module: {
    rules: [
      { test: /\.txt$/, use: 'raw-loader' }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({template: './src/index.html'})
  ]
};
module.exports = config;
````