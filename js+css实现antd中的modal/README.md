# js+css实现modal


#### 页面截图

只截取了一部分，弹窗其实是垂直居中的
![avatar](https://crm-demo1.oss-cn-beijing.aliyuncs.com/images/%E5%BC%B9%E7%AA%97.png)

#### 实现核心描述

整个页面规定两个div一个是整个body的div，一个是弹窗的div，当点击按钮时，弹窗的div显示，body的div背景置灰；当点击退出弹窗时，弹窗div消失，body背景正常
注：css中写了大量的transform，scale等都是为了让弹窗的显示比较的平滑，比如弹窗进入或关闭时带些缩放的功能等

#### 实现代码
```
<!DOCTYPE html>
<!-- saved from url=(0112)https://googlesamples.github.io/web-fundamentals/fundamentals/design-and-ux/animations/modal-view-animation.html -->
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width">
    <title>Simple Box Move</title>


    <style>
        /* 全局样式或其他和功能无关的样式，可忽略 */
        html,
        body {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
            font-family: 'Helvetica', 'Arial';
        }
        header {
            height: 50px;
            background: #3C78D8;
            width: 100%;
            line-height: 50px;
            text-align: right;
            margin: 0 auto
        }



        /* 点击按钮的样式，可忽略 */
        .dont-press-me-button {
            background: #FFF;
            border: none;
            color: #333;
            font-size: 13px;
            padding: 6px 10px;
            border-radius: 2px;
            margin-right: 10px;
        }



        /* 接下来是和弹窗有关的样式，很重要，根据html页面的div数，
        可以推断出整个弹框是由三个容器包含着的，
        最外层的是modal-view-container，
        其次是modal view，
        最后是modal-view-details，根据名字就可以大体推断出每个部分的用处和功能 */



        <!-- 弹窗容器的样式，要注意pointer-events,元素不再是鼠标事件的目标，鼠标不再监听当前层而去监听下面的层中的元素-->
        .modal-view-container {
            width: 100%;
            height: 100%;
            overflow: hidden;
            position: relative;
            box-sizing: border-box;
            z-index: 10;
             <!-- 加上此按钮，右上角的弹窗按钮才可以触发 -->
            pointer-events: none;
            margin: 0 auto
        }
        .modal-view-container {
            position: fixed;
        }



       <!-- 弹窗的样式，下面的两个css分别是点击前弹窗的样式，和点击后弹窗的样式-->
        .view.modal {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100vh;
            z-index: 2;

            -webkit-transform: scale(1.15);
            transform: scale(1.15);

            -webkit-transition: -webkit-transform 0.1s cubic-bezier(0.465, 0.183, 0.153, 0.946),
                opacity 0.1s cubic-bezier(0.465, 0.183, 0.153, 0.946);
            transition: transform 0.1s cubic-bezier(0.465, 0.183, 0.153, 0.946),
                opacity 0.1s cubic-bezier(0.465, 0.183, 0.153, 0.946);
            pointer-events: none;
            opacity: 0;
            will-change: transform, opacity;
        }

        .view.modal.visible {
            <!-- 加上此按钮，弹窗取消按钮(sorry)才可以触发 -->
            pointer-events: auto;
            opacity: 1;
            -webkit-transform: scale(1);
            transform: scale(1);

            -webkit-transition: -webkit-transform 0.3s cubic-bezier(0.465, 0.183, 0.153, 0.946),
                opacity 0.3s cubic-bezier(0.465, 0.183, 0.153, 0.946);
            transition: transform 0.3s cubic-bezier(0.465, 0.183, 0.153, 0.946),
                opacity 0.3s cubic-bezier(0.465, 0.183, 0.153, 0.946);
        }




        <!-- 弹窗内容的样式，包括弹窗的标题，段落，按钮等样式 -->
        .modal-view-details {
            background: #FFF;
            z-index: 2;
            position: fixed;
            left: 50%;
            top: 50%;
            padding: 25px 25px 50px 25px;
            width: 650px;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            border-radius: 2px;
            box-shadow: 0 10px 20px rgba(0, 0, 0, 0.6);
        }

        .modal-view-details h1 {
            margin: 0.2em 0 0.4em 0;
            padding: 0;
            line-height: 1;
            font-size: 24px;
            color: #111;
        }

        .modal-view-details p {
            font-size: 14px;
            line-height: 1.6;
            margin: 0 0 1em 0;
            color: #666;
        }

        .modal-view-details button {
            position: absolute;
            bottom: 10px;
            right: 10px;
            background: none;
            color: #3C78D8;
            border-radius: 2px;
            border: none;
            font-size: 16px;
            padding: 6px 10px;
            text-transform: uppercase;
        }



       <!-- modal类点击后的样式，北京被置灰之类的操作 -->
        .modal:after {
            position: fixed;
            background: rgba(0, 0, 0, 0.4);
            width: 100%;
            height: 100%;
            left: 0;
            top: 0;
            display: block;
            content: '';
        } 
    </style>


</head>
<body>
    <!-- 弹框按钮，点击后弹出弹框 -->
    <header>
        <button class="dont-press-me-button">Don't press me.</button>
    </header>

    <!-- 弹框所占的div及其内容 -->
    <div class="modal-view-container">
        <div class="modal view">
            <div class="modal-view-details">
                <h1>Oh no!</h1>
                <p>You pressed the button! Well that's gone and torn it, hasn't it?</p>
                <button>Sorry</button>
            </div>
        </div>
    </div>





    <script>
       // 虚拟dom，拿到弹框所占div，所在的类，方便后续对类进行add和remove操作
        var modal = document.querySelector('.modal');


        //点击其他按钮“sorry”弹窗类名移除了viseble，弹窗不显示
        function onModalDismissPress(evt) {
            modal.classList.remove('visible');
        }
        //为所有button(本例中可用了只有sorry按钮可用，弹窗按钮点击后就被禁用了，只剩下弹窗中的按钮可用)增加了事件监听，点击后弹窗消失
        var modalDismissButton = modal.querySelector('button');
        modalDismissButton.addEventListener('click', onModalDismissPress);


        //点击按钮“Don't press me”，类名增加了visible，弹窗显示
        function onDontPressPress(evt) {
            modal.classList.add('visible');
        }
        //弹窗按钮(为类名为dont-press-me-button的增加了点击事件)，点击按钮弹出弹窗
        var dontPressMeButton = document.querySelector('.dont-press-me-button');
        dontPressMeButton.addEventListener('click', onDontPressPress);

    </script>


</body>

</html>
```