# 进入全屏功能实现
项目基于react + antd + js实现

##### 首先我们可以在公共组件库中写下如下代码
```
import React from "react";
import { FullscreenOutlined } from "@ant-design/icons";
import { Button } from "antd";

export default class FullScreen extends React.Component {
  state = { isFullScreen: false };

  componentDidMount() {
    this.watchFullScreen();
  }

  handleFullScrren = () => {
    !this.state.isFullScreen ? this.requestFullScreen() : this.exitFullscreen();
  };

  // 进入全屏
  requestFullScreen = () => {
    if (!this.props.virtualDom) {
      return;
    }
    let de = this.props.virtualDom;
    if (de.requestFullscreen) {
      de.requestFullscreen();
    } else if (de.mozRequestFullScreen) {
      de.mozRequestFullScreen();
    } else if (de.webkitRequestFullScreen) {
      de.webkitRequestFullScreen();
    }
  };
  // 退出全屏
  exitFullscreen = () => {
    let de = document;
    if (de.exitFullscreen) {
      de.exitFullscreen();
    } else if (de.mozCancelFullScreen) {
      de.mozCancelFullScreen();
    } else if (de.webkitCancelFullScreen) {
      de.webkitCancelFullScreen();
    }
  };

  // 监听fullscreenchange事件
  watchFullScreen = () => {
    document.addEventListener(
      "fullscreenchange",
      () => {
        this.setState({ isFullScreen: document.fullscreen });
      },
      false
    );
    document.addEventListener(
      "mozfullscreenchange",
      () => {
        this.setState({ isFullScreen: document.mozFullScreen });
      },
      false
    );
    document.addEventListener(
      "webkitfullscreenchange",
      () => {
        this.setState({ isFullScreen: document.webkitIsFullScreen });
      },
      false
    );
  };

  render() {
    return (
      <Button type="primary" onClick={this.handleFullScrren}>
        进入全屏
      </Button>
    );
  }
}
```
#### 然后在需要全屏展示的部分写下如下代码
```
import FullScreen from "components/FullScreen";//引入组件
//在合适的位置写下子组件，全屏功能一般放在右上角位置
  <div className="full-screen">
                <FullScreen virtualDom={this.state.virtualDom} />
  </div>
//项目初始化时获取要全屏展示的区域部门的dom元素
  componentDidMount() {
    this.setState({
      virtualDom: document.documentElement.getElementsByClassName("map")[0]
    });
  }
```