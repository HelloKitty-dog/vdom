# React.Fragment
React 中常见模式是为一个组件返回多个元素(比如antd的render函数)。为了包裹多个元素你肯定写过很多的 div 和 span，进行不必要的嵌套，无形中增加了浏览器的渲染压力。

为了让render函数返回多个元素我们有如下的处理方式：
### 一.使用div包一下
```
 return (
        <div>
            <div>一步 01</div>
            <div>一步 02</div>
            <div>一步 03</div>
            <div>一步 04</div>
        </div>
    );
    ```

### 二.使用数组返回

```
 return [
        <div>一步 01</div>,
        <div>一步 02</div>,
        <div>一步 03</div>,
        <div>一步 04</div>
    ];
    ```
### 三.使用React.Fragment返回
```
 return (
        <React.Fragment>
            <div>一步 01</div>
            <div>一步 02</div>
            <div>一步 03</div>
            <div>一步 04</div>
        </React.Fragment>
    );
        ```

#### Fragments简写形式<></>简写形式<></>目前有些前端工具支持的还不太好，用 create-react-app 创建的项目就不能通过编译,大家使用的时候要小心注意。