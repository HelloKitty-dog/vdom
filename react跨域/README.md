# react跨域

React中也会遇到跨域问题，和其他解决方式大致相同，也是通过修改proxy来实现方法主要有两种
1.在package.json中直接添加 "proxy": "http://localhost:4000”,这样便可以实现跨域，但是更好的方法是第二种
2.通过在src中另建一个文件，在文件中引入
```
const proxy = require('http-proxy-middleware'); http…是需要通过npm安装的，
```

```
module.exports = function(app) {
  app.use(
    '/api',
    proxy({
      target: 'http://10.100.71.179:5000',
      pathRewrite: {
        '^/api/': '/', // rewrite path
      },
      changeOrigin: true,
    })
  );
};
```
通过这种方式来实现跨域，
其中pathRewrite是为了重写路径，即把/换成/api

注意：实现跨域的时候如果不用pathRewrite来重新api，那么自己写的接口中必须要带有api/v1/…因为api是实现proxy的唯一标识，也就是说
只有路径中遇到了api才能出发proxy进而实现跨域

