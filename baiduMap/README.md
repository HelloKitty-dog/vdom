# 百度地图自动检索
虽然ts的类型校验对我们来说提供了不少的便利，但是由于一些框架文档的缺失导致ts使用起来并不是特别的顺手，今天记录下一个百度地图在ts中使用自动搜索定位的按理

### html的部分，对应的就是antd return中的部分
由于百度地图的api是使用的原生的js和html需要我们手动的建立一个自动完成的对象，通过dom获取元素，这就对ts很不友好，需要判断的地方过多，理解了百度api的内涵就可以使用antd中的AutoComplete直接进行代替
```
 <AutoComplete
                style={{ width: 350, margin: '5px' }}
                onSelect={onSelect}
                onSearch={onSearch}
                allowClear={true}
                placeholder="输入关键字"
            >
                {optionsList.map((item: any) => (
                    <Option key={item.uid} value={item.value}>
                        {item.label}
                    </Option>
                ))}
</AutoComplete>
```
### 方法部分
#### onSearch
onSearch部分，参数可以直接拿到，输入关键字即可实时执行该方法
```  
const onSearch = (searchText: string) => {
        var local = new window.BMap.LocalSearch(map);
        //检索结束后的回调方法
        local.setSearchCompleteCallback((results: any) => {
            if (local.getStatus() === 0) {
                let suggestionList: any = []
                const list = results.Br
                const len = list.length
                for (var i = 0; i < len; i++) {
                    var { address, title, point, uid } = list[i];
                    const label = `${title}${address ? '(' + address + ')' : ''}`
                    suggestionList.push({ label: label, point, value: label, uid })
                }
                if (suggestionList.length > 0) {
                    setOptionsList(suggestionList)
                } else {
                    message.error('未查询到相关结果，请尝试其他关键字')
                }
            }
        })
        local.search(searchText);
    }
```
### onSelect
这部分功能主要实现了点击后将目标位置置为地图中心点，并且通过marker进行定位，如果定位不准确可以我们又增加了拖拽功能可以对marker点进行微调
```
  const onSelect = (val: string) => {
        map.clearOverlays()
        const cur: any = optionsList.find((o: any) => o.value === val);
        if (cur) {
            map.centerAndZoom(cur.point, 18)
                 //定义marker图标
            const icon = new window.BMap.Icon(
                require('@assets/imgs/home/icon/sg-bike.png'),
                new window.BMap.Size(36, 41),
                {
                    size: new window.BMap.Size(36, 41)
                }
            )
            //marker定义
            const marker = new window.BMap.Marker(
                cur.point,
                {
                    enableDragging: true,
                    offset: new window.BMap.Size(0, -20),
                    icon
                }
            )
            //marker绘制
            map.addOverlay(marker)
            const attribute=()=> {
                setPoint(marker.point)
            }
            marker.addEventListener("dragend", attribute);
        }
};
```
