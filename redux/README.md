# redux源码解析

![avatar](https://crm-demo1.oss-cn-beijing.aliyuncs.com/images/bg2016091802.jpg)

### 使用redux必须要有的部分：

①必须用connect连接UI组件和逻辑组件，具体写法
```
connect(mapStateToProps, mapDispatchToProps)(UI组件名)
```

②UI组件，一般可以写成一个类，只是为了显示数据，里面会有一些方法、html元素等来展示数据和一些按钮
* 		只负责 UI 的呈现，不带有任何业务逻辑
* 		没有状态（即不使用this.state这个变量）
* 		所有数据都由参数（this.props）提供
* 		不使用任何 Redux 的 API

③mapStateToProps：建立一个从state对象到props对象的映射关系。比如value：state.count

④mapDispatchToProps：建立 UI 组件的参数到store.dispatch方法的映射。比如
```
const mapDispatchToProps = dispatch => {
  return {
    onTodoClick: id => {
      dispatch(toggleTodo(id))
    }
  }
  ```
其中onTodoClick是UI组件的函数，dispatch里面是一个action

⑤rudecer函数：Reducers 指定了应用状态的变化如何响应 actions 并发送到 store 的，记住 actions 只是描述了有事情发生了这一事实，并没有描述应用如何更新 state。
\比如
```
function reducer(state = { count: 0 }, action) {
  const count = state.count
  switch (action.type) {
    case 'increase':
      return { count: count + 1 }
    default:
      return state
  }
}
```
⑥创建store对象：
```
const store=createStore(reducer,middleware)
```
createStore函数的参数必须有reducer相关的方法，可以加入相关中间件来实现异步操作或者是改变store.dispatch()的返回类型(默认只能返回对象)，以及增加一些别的功能等

⑦Provider：Provider组件，可以让容器组件拿到store中的全部信息，Provider在根组件外面包了一层，这样一来，App的所有子组件就默认都可以拿到store中的信息了，而不必用export特意导出。比如
```
 <Provider store={store}>
    <App />
  </Provider>
  ```

### 使用redux可以有的部分：

①Action 生成器：主要是为了进一步分解dispatch方法，因为dispatch的参数是一个对象，默认会有type和state两个参数type表示action的名字，要与reducer中的名字一一对应，这样才能进行指定的操作。比如，如果你定义了action生成器
```
const increaseAction = { type: 'increase' } 
```
这样我们的dispatch方法就可以这样写 
```
dispatch(increaseAction)
```
  注：这个是没有state初始值的时候。

②UI组件验证器propTypes：因为UI组件中设计到一些直接显示的数据和一些点击方法之类的操作，需要我们使用propTypes来指定他的类型.

使用方式:
组件名称.propTypes = {
key1:验证器,
}

支持的验证类型: array，bool，func，number，object，string

其他作用：

指定数据类型成数组:
React.PropTypes.arrayOf(React.PropTypes.number)

指定数据类型到对象:
React.PropTypes.objectOf(React.PropTypes.number)
￼


### 总的源码如下：
```
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import { createStore } from 'redux'
import { Provider, connect } from 'react-redux'

// React component
class Counter extends Component {
  render() {
    const { value, onIncreaseClick } = this.props
    return (
      <div>
        <span>{value}</span>
        <button onClick={onIncreaseClick}>Increase</button>
      </div>
    )
  }
}
//代码验证器
Counter.propTypes = {
  value: PropTypes.number.isRequired,
  onIncreaseClick: PropTypes.func.isRequired
}

// Action
const increaseAction = { type: 'increase' }

// Reducer
function counter(state = { count: 0 }, action) {
  const count = state.count
  switch (action.type) {
    case 'increase':
      return { count: count + 1 }
    default:
      return state
  }
}

// Store
const store = createStore(counter)

// Map Redux state to component props
function mapStateToProps(state) {
  return {
    value: state.count
  }
}

// Map Redux actions to component props
function mapDispatchToProps(dispatch) {
  return {
    onIncreaseClick: () => dispatch(increaseAction)
  }
}

// Connected Component
const App = connect(
  mapStateToProps,
  mapDispatchToProps
)(Counter)

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
```