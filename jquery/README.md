# jquery
由于jquery和js有很多通性，所以只在这里记录一些常用的方法和简单的例子，作为开发的工具使用

### 文档就绪(相当于js中的window.onload):
```
$(document).ready(function(){
 
   // 开始写 jQuery 代码...
 
});
```
可简写为
```
$(function(){
 
   // 开始写 jQuery 代码...
 
});
```
### jquery 选择器
```
语法	                   描述	    
$("*")	                    选取所有元素	
$(this)	                    选取当前 HTML 元素	
$("p.intro")	            选取 class 为 intro 的 <p> 元素	
$("p:first")	            选取第一个 <p> 元素	
$("ul li:first")	        选取第一个 <ul> 元素的第一个 <li> 元素	
$("ul li:first-child")  	选取每个 <ul> 元素的第一个 <li> 元素	
$("[href]")	                选取带有 href 属性的元素	
$("a[target='_blank']")     选取所有 target 属性值等于 "_blank" 的 <a> 元素
$("a[target!='_blank']")	选取所有 target 属性值不等于 "_blank" 的 <a> 元素	
$(":button")	            选取所有 type="button" 的 <input> 元素 和 <button> 元素	
$("tr:even")	            选取偶数位置的 <tr> 元素	
$("tr:odd")	                选取奇数位置的 <tr> 元素
```

### jquery事件
```
鼠标事件	 键盘事件	   表单事件	     文档/窗口事件
click	    keypress	submit	     load
dblclick	keydown	    change	     resize
mouseenter	keyup	    focus	     scroll
mouseleave	 	        blur	     unload
hover	 
```	 	 
### jquery效果

+ 隐藏/显示

$(selector).hide(speed,callback);

$(selector).show(speed,callback);

+ 淡入/淡出

$(selector).fadeIn(speed,callback);

$(selector).fadeOut(speed,callback);

+ 滑动

$(selector).slideDown(speed,callback);

$(selector).slideUp(speed,callback);

+ 动画

$(selector).animate({params},speed,callback);

必需的 params 参数定义形成动画的 CSS 属性。

可选的 speed 参数规定效果的时长。它可以取以下值："slow"、"fast" 或毫秒。

可选的 callback 参数是动画完成后所执行的函数名称。

下面的例子演示 animate() 方法的简单应用。它把 <div> 元素往右边移动了 250 像素：

实例
```
$("button").click(function(){
  $("div").animate({left:'250px'});
});
```

+ 停止动画

$(selector).stop(stopAll,goToEnd);

可选的 stopAll 参数规定是否应该清除动画队列。默认是 false，即仅停止活动的动画，允许任何排入队列的动画向后执行。

可选的 goToEnd 参数规定是否立即完成当前动画。默认是 false。

因此，默认地，stop() 会清除在被选元素上指定的当前动画。

+ 回调函数
```
使用 callback 实例
$("button").click(function(){
  $("p").hide("slow",function(){
    alert("段落现在被隐藏了");
  });
});
```
+ 链式调用
jquery支持链式调用
```
$("#p1").css("color","red").slideUp(2000).slideDown(2000);
```
### jquery捕获、设置

获得内容：
+ text() - 设置或返回所选元素的文本内容
+ html() - 设置或返回所选元素的内容（包括 HTML 标记）
+ val() - 设置或返回表单字段的值

若果括号内添加相应的字符串内容，那么即可设置相应元素的属性

获得属性
+ attr() 方法用于获取属性值。

### 添加元素

我们将学习用于添加新内容的四个 jQuery 方法：

+ append() - 在被选元素的结尾插入内容
+ prepend() - 在被选元素的开头插入内容
+ after() - 在被选元素之后插入内容
+ before() - 在被选元素之前插入内容

# 删除元素
下列二者的主要区别就是会不会删除所选元素
+ remove() - 删除被选元素（及其子元素）
+ empty() - 从被选元素中删除子元素

### CSS类
+ addClass() - 向被选元素添加一个或多个类
+ removeClass() - 从被选元素删除一个或多个类
+ toggleClass() - 对被选元素进行添加/删除类的切换操作
+ css() - 设置或返回样式属性

### jquery尺寸

![avatar](https://crm-demo1.oss-cn-beijing.aliyuncs.com/images/img_jquerydim.gif)

### jquery遍历

+ parent()：返回被选元素的直接父元素。
+ parents()：返回被选元素的所有祖先元素，它一路向上直到文档的根元素 (<html>)。
+ parentsUntil()：返回介于两个给定元素之间的所有祖先元素。
